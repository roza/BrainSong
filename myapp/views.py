from .app import *
from flask import url_for, redirect, render_template, request, flash, Response
import flask
from .models import *
from .formulaireModele import *
from flask.ext.mail import Message
from .modelGraph import *
from .modifMusic import speedModifier
from .conversionMusique import *
from werkzeug.datastructures import ImmutableMultiDict
import os
@app.route("/")
def home():
	connect=LoginForm()
	return render_template("home.html", connect=connect)


from flask.ext.login import login_user, logout_user, current_user
from flask import request
@app.route("/login/<path:code>", methods=("POST",))
def connect(code=None):
	connect=LoginForm()
	if connect.validate_on_submit():
		user=connect.get_authenticated_user()
		if user:
			login_user(user)
			flash(u'Vous êtes maintenant connecté.','connectOk')
			if(code==None or code=="root" or code=="graphiques"):
				return redirect(url_for("home"))
			else:
				return redirect(code)
	flash(u'Votre login ou votre mot de passe est incorrect.','erreurConnexion')
	if(code==None or code=="root" or code=="graphiques"):
		return redirect("/#section")
	else:
		return redirect(code+"#section")

@app.route("/signin/")
@app.route("/signin/<path:code>")
@app.route("/signin/<path:code>",methods=("POST",))
def signin(code=None):
	connect=LoginForm()
	inscription=InscriptionForm()
	if inscription.validate_on_submit():
		if inscription.usernameSignin.data!="":
			pseudoOk=inscription.pseudo_valide()
			if(pseudoOk==None):
				if(inscription.passwordSignin.data!=""):
					if(inscription.passwordSignin.data==inscription.confPassword.data):
						newUser(inscription.usernameSignin.data, inscription.passwordSignin.data)
						flash(u'Votre compte à bien été créer','connectOk')
						if(code==None or code=="root"):
							return redirect(url_for("home"))
						else:
							return redirect(code)
					else:
						flash(u'Les deux mots de passes doivent être identiques','signin')
				else:
					flash(u'Le mot de passe ne peut etre vide','signin')
			else:
				flash(u'Ce pseudo est deja pris','signin')
		else:
			flash(u'Le pseudo ne peut etre vide','signin')
	return render_template("signin.html",connect=connect, inscription=inscription)

@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for("home"))

@app.route("/graphiques/")
def graphiques():
	connect=LoginForm()
	return render_template("graphiques.html",connect=connect)

import time
@app.route("/recherche/")
@app.route("/recherche/", methods=("POST",))
def recherche(affinage=False):
	form=RechercheForm()
	connect=LoginForm()
	reponse=None
	date=None
	if form.is_submitted():
		if(form.recherche.data!=None and len(form.recherche.data)!=0):
			if(form.affinage.data): #Recherche avec filtre
				affinage=True
				if(form.dateNumber.data!=None):
					date=int(time.mktime(time.strptime(form.dateNumber.data, '%d/%m/%Y'))) - time.timezone
				if(form.typeRecherche.data=='Tag'):
					if(form.date.data=="exactement le"):
						reponse=conversionListTuple_4args(form.recherche.data, date, rechercheToutTag,rechercheDateEqEt)
					elif(form.date.data=="avant le"):
						reponse=conversionListTuple_4args(form.recherche.data, date,rechercheToutTag, rechercheDateInfEt)
					elif(form.date.data=="après le"):
						reponse=conversionListTuple_4args(form.recherche.data, date, rechercheToutTag, rechercheDateSupEt)
					else:
						reponse=conversionListTuple_RechercheSimpleDeco(form.recherche.data,  rechercheToutTag)
				else:
					if(form.date.data=="exactement le"):
						reponse=conversionListTuple_4args(form.recherche.data, date,rechercheToutTitre, rechercheDateEqEt)
					elif(form.date.data=="avant le"):
						reponse=conversionListTuple_4args(form.recherche.data, date,rechercheToutTitre, rechercheDateInfEt)
					elif(form.date.data=="après le"):
						reponse=conversionListTuple_4args(form.recherche.data, date, rechercheToutTitre, rechercheDateSupEt)
					else:
						reponse=conversionListTuple_RechercheSimpleDeco(form.recherche.data, rechercheToutTitre)
			else: #recherche de base
				reponse=conversionListTuple_RechercheSimpleDeco(form.recherche.data, rechercheToutTitre)
		else:
			flash(u'Le champ recherche ne peut être vide','erreurRecherche')
	return render_template("recherche.html",form=form, connect=connect, date=date, reponse=reponse, affinage=affinage)

@app.route("/mentions/")
def mentions():
	connect=LoginForm()
	return render_template("mentions.html", connect=connect)

@app.route("/contact/")
def contact():
	connect=LoginForm()
	form=ContactForm()
	return render_template("contact.html",form=form, connect=connect)

@app.route("/envoyerContact/", methods=("POST",))
def envoyerContact():
	connect=LoginForm()
	form=ContactForm()
	if(form.titre.data==None or len(form.titre.data)==0):
		flash(u'Le message doit avoir un titre','erreurContact')
	elif(form.email.data==None or len(form.email.data)==0):
		flash(u'Il faut une adresse email non vide','erreurContact')
	elif(form.message.data==None or len(form.message.data)==0):
		flash(u'Il faut un message non vide','erreurContact')
	else:
		envoyerMessage(form.titre.data, form.email.data, form.message.data)
		flash(u'Message envoyé','connectOk')
	return render_template("contact.html",form=form, connect=connect)

def envoyerMessage(titre, email, message):
	texte="Cet email est envoyé par <b>"+email+"</b> depuis le site <b>BrainSong</b> grâce au formulaire de contact :<br><br>"+message
	msg = Message(subject=titre, html=texte,recipients=["clement.mereau@etu.univ-orleans.fr"])#,"anais.lavorel@etu.univ-orleans.fr","paul-antoine.dumergue@etu.univ-orleans.fr","antoine.lamirault@etu.univ-orleans.fr","titouan.lazard@etu.univ-orleans.fr","florent.alapetite@etu.univ-orleans.fr"])
	mail.send(msg)

@app.route("/upload/")
def upload():
	connect=LoginForm()
	form=UploadForm()
	return render_template("dataupload.html",form=form,connect=connect)

@app.route("/checkUser/<user>/<passwd>", methods=("GET","POST"))
@app.route("/checkUser/")
def check(user, passwd):
	connect=LoginForm()
	message=""
	if user!="" and passwd!="":
		connect.username.data=user
		connect.password.data=passwd
		user=connect.get_authenticated_user()
		if user:
			message="true"
		else:
			message="false"
	return render_template("checkUser.html",connect=connect, message=message)


@app.route("/envoyerFichier/", methods=("POST",))
def uploadFichier():
	if request.method == 'POST':
		file = request.files['fichier']
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			user=get_User(request.form.getlist("username")[0],request.form.getlist("password")[0])
			if(user!=None):
				id=get_IdDispo(Record)
				reecriture(file,id)
				newRecord(file.filename[:-4], user.id ,2, ["cours"], int(time.time()), "/myapp/uploads/brut"+str(id)+".csv", "/myapp/uploads/courbes"+str(id)+".csv")
				return ""
			else:
				print("mauvais couple user mdp")
		else:
			print("format invalide")
	return ""
@app.route("/graphiques/", methods=["POST"])
@app.route("/graphiques/<int:id>", methods=["GET"])
def graphique(listeid=None,id=None):
	connect=LoginForm()
	if request.method == 'POST':
		listeid=request.form.getlist("choix")
	else:
		listeid=[id]
	listeEnr=[get_ElementsById(Record, x) for x in listeid]
	listename=[x.name for x in listeEnr]
	return render_template("graphiques.html",connect=connect,listeEnr=listeEnr,listeid=listeid,listename=listename)

import datetime
@app.template_filter('timeStampToDate')
def timeStampToDate(timer):
	return datetime.datetime.fromtimestamp(int(timer)).strftime('%d/%m/%Y')

@app.route("/mesGraphiques/")
def mesEnregistrements():
	musiqueForm=UploadMusiqueForm()
	connect=LoginForm()
	graphiques=get_RecordUser(current_user.id)
	liste=get_Elements(Record)
	listeSon=[]
	for elem in liste:
		if elem.owner==current_user.id and elem.lienMusiqueModif!=None:
			listeSon.append(elem)
	return render_template("mesGraphiques.html", graphiques=graphiques, connect=connect, musiqueForm=musiqueForm, listeSon=listeSon)

@app.route("/graphiques/<name>")
def csv(name):
	test=open(os.getcwd()+"/myapp/uploads/"+name).read()
	return Response(test,mimetype='text/csv')

@app.route('/api/modifEnregistrement', methods=['POST'])
def modifEnregistrement():
	if not request.json or not 'id' in request.json:
	 	abort(400)
	enregistrement = get_ElementsById(Record, request.json['id'])
	enregistrement.name = request.json['name']
	enregistrement.visibility = request.json.get('visibility')
	enregistrement.tags = []
	for tag in request.json.get('tags'):
		enregistrement.tags.append(Tag(name=tag));
	db.session.commit()
	return jsonify(enregistrement.to_json())

@app.route('/api/deleteEnregistrement', methods=['POST'])
def deleteEnregistrement():
        if not request.json or not 'id' in request.json:
        	abort(400)
        enregistrement = get_ElementsById(Record, request.json['id'])
        os.remove(mkpath("../myapp/uploads/courbes"+str(request.json['id'])+".csv"))
        os.remove(mkpath("../myapp/uploads/brut"+str(request.json['id'])+".csv"))
        db.session.delete(enregistrement)
        db.session.commit()
        return jsonify({'id': request.json['id']})

import copy
@app.route('/api/deleteTag', methods=['POST'])
def deleteTag():
	if not request.json or not 'id' in request.json:
	 	abort(400)
	enregistrement = get_ElementsById(Record, str(request.json['id']))
	lastListe=copy.copy(enregistrement.tags)
	enregistrement.tags = []
	for tag in lastListe:
		if(tag.name!=request.json['valueTag']):
			enregistrement.tags.append(tag);
	db.session.commit()
	return jsonify({'id': request.json['id'], 'valueTag': request.json['valueTag']})

@app.route('/api/addMusique', methods=['POST'])
def addMusique():
	file = request.files['fichier']
	graphiques=get_RecordUser(current_user.id)
	connect=LoginForm()
	form=UploadMusiqueForm()
	musiqueForm=UploadMusiqueForm()
	if musiqueForm.typeMusique.data =="Création":
		creationMidi(form.id.data, "creation"+form.id.data+"02.mid",extraction(os.getcwd()+'/myapp/uploads/courbes'+form.id.data+'.csv'), [0, 0,49,49], 2)
		flash(u'Fichier bien créé','uploadOk')
	else:
		if file and allowed_file(file.filename):
			file.save(os.getcwd()+"/myapp/static/musique/brut.wav")
			url=speedModifier(file.name, os.getcwd()+"/myapp/static/musique/brut50.wav",form.id.data)
			flash(u'Fichier bien envoyé et modifié','uploadOk')
		else:
			flash(u'Mauvais format ou fichier inexistant','uploadFail')
	liste=get_Elements(Record)
	listeSon=[]
	for elem in liste:
		if elem.owner==current_user.id and (elem.lienMusiqueModif!=None or elem.lienMusiqueGen!=None):
			listeSon.append(elem)
	return redirect(url_for('mesEnregistrements'))

@app.route("/manuel")
def manuel():
	return render_template("/Template/Base.html", connect=connect)

@app.route("/getbrainsong")
def getbrain():
	return flask.send_file("static/brainsong.apk", as_attachment = True, cache_timeout = 360)