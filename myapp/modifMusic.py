from .models import *
from scipy.io.wavfile import read,write
from numpy import *
from .app import mkpath
import random

def readMusique(nomfic):
	return read(os.getcwd()+nomfic)

def getCourbes(nomfic):
	"""
	Permet d'ouvrir le fichier csv pour en ressortir les deux coubres Attention et Méditation
	"""
	fichier=open(os.getcwd()+nomfic, "r")
	courbes=fichier.read().split("\n")
	return courbes

def getAttentionMeditation(courbes):
	"""
	Permet d'obtenir les différentes valeurs de l'attention
	"""
	resAtt=[]
	resMed=[]
	for x in range(1,len(courbes)):
		ligne=courbes[x].split(",")
		resAtt.append(int(ligne[1]))
		resMed.append(int(ligne[2]))
	return resAtt,resMed

def speedModifier(name, datafic,id):
	"""
	Accélère ou ralenti la musique en fonction des différentes valeurs de l'attention
	"""
	rate, signal= readMusique("/myapp/static/musique/brut.wav")
	objet=get_ElementsById(Record, id)
	print(objet.lienTraite)
	courbes=getCourbes(objet.lienTraite)
	attention,meditation=getAttentionMeditation(courbes)
	n=signal.shape[0]//len(attention)
	listeDel=[]
	listeAdd=[]
	for i in range(len(attention)):
		deb=i*n
		fin=(i+1)*n
		if i==len(attention)-1:
			fin=signal.shape[0]
		for x in range(deb,fin):
			if x%((attention[i]//5)+1)==0:
				if attention[i]>50:
					listeDel.append(x)
				else:
					listeAdd.append(x)
	listeValue=[signal[x] for x in listeAdd]
	res=insert(signal,listeAdd,listeValue,0)
	lien=mkpath("../myapp/static/musique/modification"+str(id)+".wav")
	write(lien,rate,delete(res,listeDel,0))
	objet.lienMusiqueModif="/"+lien
	db.session.commit()
	return "./static/musique/modification"+str(id)

