from .app import db
from wtforms import FileField,TextAreaField,PasswordField, StringField, HiddenField, BooleanField, IntegerField, RadioField
from flask.ext.wtf import Form
from werkzeug.datastructures import MultiDict
from wtforms.validators import *
from .models import *


class ContactForm(Form):
	titre=StringField('Titre', validators=[DataRequired()])
	email=StringField('Votre email', validators=[DataRequired()])
	message=TextAreaField('Message', validators=[DataRequired()])

class LoginForm(Form):
	username=StringField('Login')
	password=PasswordField('Mot de passe')

	def get_authenticated_user(self):
		return get_User(self.username.data, self.password.data)

class InscriptionForm(Form):
	usernameSignin=StringField('Login')
	passwordSignin=PasswordField('Mot de passe')
	confPassword=PasswordField('Confirmation mot de passe')

	def pseudo_valide(self):
		return getuser(self.usernameSignin.data)

import time
class RechercheForm(Form):
	recherche=StringField('Recherche')
	affinage = BooleanField('Affiner la recherche :')
	typeRecherche=RadioField('Type de recherche :', choices=[ ('Nom de l\'enregistrement','Nom de l\'enregistrement'), ('Tag','Tag')], default='Nom de l\'enregistrement')
	date= RadioField('Date d\'enregistrement :', choices=[('exactement le','exactement le'), ('avant le','avant le'), ('après le','après le')])
	dateNumber=StringField('Date',default=time. strftime("%d/%m/%Y", time.gmtime()))

class UploadForm(Form):
	username=StringField('Login')
	password=PasswordField('Mot de passe')
	fichier=FileField('Fichier')

class UploadMusiqueForm(Form):
	fichier=FileField('Fichier audio')
	id=HiddenField(id="identifiant")
	typeMusique=RadioField('Type de musique', choices=[ ('Création','Création'), ('Modification','Modification')], default='Création')
