from __future__ import division
import collections
import itertools
from random import choice
from pyknon.genmidi import Midi
from pyknon.music import Note, NoteSeq, Rest
import csv, random
import os
from .models import *

def extraction(nomFichier):
    """Extrait les valeurs du fichier csv et retourn une liste contenant les valeurs correspondant à l'attention et la meditation"""
    fileCsv = open(nomFichier, 'r')
    with fileCsv as csvfile:
        reader = csv.DictReader(csvfile)
        Attentions = []
        Meditation = []
        for row in reader:
            if(row['attentionValue'] !=None):
                Attentions.append(int(row['attentionValue']))
                Meditation.append(int(row['meditationValue']))
            else:
                break
    return [Attentions,Meditation]

def conversion(ArrayListValeur, maximum,maxValeur ):

    """Modifie les valeurs d'une liste passer en paramettre pour les faire entrer dans l'interval [0, maxValeur[
    maximum = valeur maximal resencée pour le type de courbe
    maxValeur = la valeur maximal pour la sortie (=borne superieur)
    """
    discriminant = maximum//maxValeur
    resultat = []
    for valeur in ArrayListValeur:
        resultat.append(valeur//discriminant)
    return resultat



def creationNoteList(listeValeurs):
    """transforme une liste d'integer en liste de note"""
    resultat = []
    listeValeurs = conversion(listeValeurs, 110, 11)
    #listeOctave = conversion(listeValeurs, 12)
    #listVolume = conversion(listeValeurs, 127)
    for i in range(len(listeValeurs)):
            dur = 0.25
            volume = random.randint(100,127)
            #octave = random.randint(1,12)
            resultat.append(Note(value=listeValeurs[i],volume=volume, dur=dur))
    return resultat


#a améliorer en evitant les redondances de notes
def harmonizationMaison(listeNotes, degreHarmonisation):
    """Permet d'armoniser un ensemble de note en crean x note intermediare entre chaque (x = degreHarmonisation )"""
    resultat = []
    for i in range(len(listeNotes)-1):
        if(listeNotes[i].value != listeNotes[i+1].value):
            resultat.append(listeNotes[i])
        for j in range(1,degreHarmonisation):
            if(abs(listeNotes[i].value- listeNotes[i+1].value) >2):
                resultat.append(Note(value=(listeNotes[i].value*(degreHarmonisation-j)+listeNotes[i+1].value*j )//(degreHarmonisation),
                    volume=random.randint(100,160)))
    resultat.append(listeNotes[-1])
    return resultat


def moyenneListe(liteEntier):
    res = 0
    nb = 0
    for element in liteEntier:
        res +=element
        nb +=1
    return res//nb

def calculTempo(listeListeValeurs):
    """ cette fonction ne compare qu'attention et mediation et renvoie une liste de rythme plus ou moins rapide en fonctions de l'attention"""
    tempo = 120
    # On changera le tempo toutes les 20notes
    nbTempo =  len(listeListeValeurs[0])//20
    tempos = []
    for i in range(nbTempo):
        dif = moyenneListe(listeListeValeurs[0][i*20:(i+1)*20]) - moyenneListe(listeListeValeurs[1][i*20:(i+1)*20])
        tempos.append(tempo+dif)
    return tempos

# def ajoutRest(listeNotes):
#     res = []
#     for i in range(len(listeNotes)):
#         res.extend([listeNotes[i], Rest(0.25)])
#     return res

def creationMidi(id, fileName,listeListeValeurs, _instrument=[49,49,49,49], nbRep=1, duree=[0.666,2]):
    """ cree un fichier midi a partie de [[Valeurs],[Valeurs]],
                                        une liste d'instrument ou un instrument,
                                        le nombre de répétition,
                                        les temps de pose par listeValeurs """
    for nb in range(nbRep):
        for i in range(len(listeListeValeurs)):
            listeListeValeurs[i].extend(listeListeValeurs[i])
    tempoR = calculTempo(listeListeValeurs)
    midi = Midi(number_tracks=len(listeListeValeurs)*2, tempo=tempoR[0],  instrument=_instrument) #, instrument=random.randint(0,127))
    # listPartition =harmonizationMaison(creationNoteList(listeListeValeurs[0]),5)
    # seqNotes = NoteSeq(listPartition).stretch_dur(duree[0])
    # midi.seq_notes(seqNotes, track=0)
    chords = []
    for i in range(len(listeListeValeurs)):

        listPartition1 = harmonizationMaison(creationNoteList(listeListeValeurs[i]),5)
        seqNotes = NoteSeq(listPartition1).stretch_dur(duree[i])
        listPartition2 = gen_patterns(seqNotes)
        seqNotes2 = NoteSeq(listPartition2).stretch_dur(duree[i])
        midi.seq_notes(seqNotes,time =i*3, track=i)
        midi.seq_notes(seqNotes,time =i*2, track=(i+2))
        #midi.addTempo(i, 6*i, tempoR[nbModif])
        #print("track 1: ",seqNotes )
        chords.append(seqNotes.inversion_startswith(Note(2, 4)))
        chords.append(seqNotes.transposition(-12).retrograde().stretch_dur(1.25))
        chords.append(seqNotes.transposition(-1).retrograde().stretch_dur(1.25))
        midi.seq_chords(chords)
    #midi.seq_chords(chords)
    nom="/myapp/static/musique/"+fileName
    midi.write(os.getcwd()+nom)
    objet=get_ElementsById(Record, id)
    objet.lienMusiqueGen=nom
    db.session.commit()
    return None

# creationMidi("test01.mid",extraction('../data.csv'))
# creationMidi("test02.mid",extraction('../courbes10.csv'))
# creationMidi("test03.mid",extraction('../courbes11.csv'))



# de piano phase, permet la generation dune nouvelle suite de note
def gen_patterns(pattern, number_rotations=12, repeat=4):
    result = NoteSeq()
    for n in range(0, number_rotations):
        rotation = pattern.rotate(n)
        result.extend(rotation * repeat)
    return result


#creationMidi("ttttt.mid",extraction('../courbes19.csv'), [49], 2 )
