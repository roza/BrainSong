#!/usr/bin/python

import sys
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/brainsong/")

from myapp import app as application
application.secret_key = 'c6922300-2d1b-40bb-a21c-0535374c02a6'
