Pour mettre en place le site :
	- Installer un virtualenv avec les paramètres de requirements.txt


Pour lancer la création du jeux d'essai depuis le terminal:
	./manage.py loaddb myapp/enregistrement.yml myapp/users.yml

Ensuite il suffit de lancer le serveur avec:
	./manage.py runserver

Equipe projet:
Antoine Lamirault
Clément Méreau
Anaïs Lavorel